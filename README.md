# Challenge Project 01 - Mini Pokedex
Se busca que el postulante construya una sencilla aplicación web que permita visualizar una pokedex limitada y con solo algunas funciones básicas.

Adicionalmente, se busca que el postulante utilice AWS Amplify para realizar el deploy y configuración del login del proyecto.

## Mockups
La siguiente imagen muestra los mockups o vistas que se desea que el postulante construya.

- Existe una vista principal, donde se puede ver un resumen de la pokedex, con algunos datos de cada pokemon.
- Pueden filtrarse de la vista total por "type".
- Al presionar en "details" se debe abrir una ventana emergente o una nueva vista donde se muestren los detalles del pokemon elegido, en esta vista se muestran todos los detalles disponibles.
- Esta vista tiene dos pestañas: "Information" y "Evolution", por defecto debería mostrarse la vista de "Information".

![](img/img_01.png)

## Datos de trabajo
La información de la pokedex se encuentra en el archivo `pokedex.json`, esta información debe cargarse en memoria, no es necesario consumir una API para obtener la información.

## Modo de entrega
Se le entregará al postulante un repositorio en GitLab o Github donde podrá subir su proyecto, debe incluir un README y todo el código relacionado.

## Deseables mínimos a considerar
Se busca que el postulante sea capaz de entregar un proyecto consiguiendo como mínimo lo siguiente:
- seguir las buenas prácticas de desarrollo del framework o librería utilizada.
- README correctamente documentado.
  - breve descripción
  - como levantar servidor de desarrollo (incluyendo toda la configuración previa necesaria)
  - como hacer el deploy a un servidor productivo
- deploy de la app utilizando AWS Amplify (*)
- agregar una vista de login utilizando la integración de AWS Amplify con Amazon Cognito (*)

> (*) Estos puntos son deseables adicionales, no son estrictamente necesario pero si se valorará que el postulante logre estos hitos.